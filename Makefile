PLAN?=current-plan

all: init plan apply

upgrade:
	terraform init -upgrade

init:
	terraform init

plan: init
	terraform plan -out="${PLAN}"

apply:
	terraform apply "${PLAN}"

providers:
	terraform providers

destroy:
	terraform apply -destroy -auto-approve
