terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  required_version = ">= 1.5.1"
}

provider "aws" {
  region = var.aws_default_region
  default_tags {
    tags = {
      Project = "Joke phone numbers"
    }
  }
}
