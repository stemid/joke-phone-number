# Joke phone numbers

Inspired by [this repo](https://github.com/pjf/rickastley), here is the Swedish version.

* 🇸🇪 0766 86 84 36 • Rick Astley sings "Never gonna give you up".
* 🇸🇪 0766 86 18 52 • Leif G.W. Persson just being Leif.

**Disclaimer:** This project has no other aim than to bring joy to people. The cost to call the number from within Sweden should be standard outgoing call costs. I am not being paid anything, I am also not paying much to receive calls. Only about 9 SEK/month to reserve each number.

# Terraform

See ``vars.tf`` for which variables to set.

Personally I tend to use direnv and put some sensitive values into ``.envrc``.

```
export TF_VAR_aws_default_region=eu-north-1
export TF_VAR_aws_access_key=AKKXXXXvalue
export TF_VAR_aws_secret_key=$(pass aws.amazon.com/AKKXXXXvalue)
```

And a ``terraform.tfvars`` file like this.

```
files = [
  {
    name = "rick"
    filename = "Rick_Astley-Never_Gonna_Give_You_Up.mp3"
  },
  {
    name = "leffe"
    filename = "leffe01.mp3"
  }
]
```

I have to manually upload those files for now, and also manually update the scheduled job that generates the call graph with the S3 bucket.

Run Terraform with the Makefile like this.

```
make
make destroy
```

# Call log graph

![Call events over time](https://dljt3k73axq09.cloudfront.net/graph.png "Call events over time")

# Generate graph of call log

```
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ python callsgraph.py --save --user '<46elks API username>' --password '<46elks API password>'
$ aws s3 cp --acl public-read graph.png s3://rickastley/graph.png
```
