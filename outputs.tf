output "api_url" {
  value = aws_api_gateway_deployment.api.invoke_url
}

output "bucket_url" {
  value = "${format("https://%s", "${aws_cloudfront_distribution.bucket.domain_name}")}"
}

output "bucket" {
  value = "s3://${aws_s3_bucket.bucket.id}/"
}
