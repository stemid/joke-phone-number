resource "random_string" "bucket_name" {
  length = 8
  special = false
  upper = false
}

resource "aws_s3_bucket" "bucket" {
  bucket = "joke-number-${random_string.bucket_name.id}"
}

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    principals {
      type = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.bucket.iam_arn]
    }

    actions = [
      "s3:GetObject",
    ]

    resources = [
      aws_s3_bucket.bucket.arn,
      "${aws_s3_bucket.bucket.arn}/*"
    ]
  }
}

resource "aws_s3_bucket_policy" "cloudfront_policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = data.aws_iam_policy_document.bucket_policy.json
}

resource "aws_cloudfront_origin_access_identity" "bucket" {}

resource "aws_cloudfront_distribution" "bucket" {
  origin {
    domain_name = aws_s3_bucket.bucket.bucket_regional_domain_name
    origin_id   = "S3-${aws_s3_bucket.bucket.id}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.bucket.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "joke phone messages"
  #default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = "S3-${aws_s3_bucket.bucket.id}"
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400

    forwarded_values {
      query_string = true

      cookies {
        forward = "all"
      }
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  restrictions {
    geo_restriction {
      # Set to "none" and remove locations if you want no resitrctions
      restriction_type = "whitelist"
      locations = ["SE", "DE", "NL", "FR", "DK", "NO", "AT", "CH", "BG", "HR"]
    }
  }
}

data "template_file" "function" {
  count = length(var.files)
  template = file("${path.module}/files/function.js")
  vars = {
    filename = var.files[count.index].filename
    bucket_url = "${format("https://%s", aws_cloudfront_distribution.bucket.domain_name)}"
  }
}

data "archive_file" "function" {
  count = length(var.files)
  type = "zip"
  output_path = "${path.module}/files/function-${count.index}.zip"

  source {
    content = data.template_file.function[count.index].rendered
    filename = "main.js"
  }
}

resource "random_string" "function_name" {
  count = length(var.files)
  length = 8
  special = false
  upper = false
}

resource "aws_iam_role" "lambda" {
 name   = "aws_lambda_role"
 assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "function" {
  count = length(var.files)
  filename = "${path.module}/files/function-${count.index}.zip"
  function_name = "${var.files[count.index].name}-${random_string.function_name[count.index].id}"
  role = "${aws_iam_role.lambda.arn}"
  handler = "main.handler"
  source_code_hash = data.archive_file.function[count.index].output_base64sha256
  runtime = "nodejs18.x"
  description = "Joke phone number lambda"
  publish = true
}

resource "aws_lambda_permission" "function" {
  count = length(var.files)
  statement_id = "AllowAPIGatewayTo${var.files[count.index].name}-${random_string.function_name[count.index].id}"
  action = "lambda:InvokeFunction"
  function_name = "${var.files[count.index].name}-${random_string.function_name[count.index].id}"
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*"
}

# Create API Gateway with Rest API type
resource "aws_api_gateway_rest_api" "api" {
  name = "API gateway for 46elks joke-numbers"
  description = "Serverless Application using Terraform"
}

resource "aws_api_gateway_resource" "root" {
  count = length(var.files)
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id = aws_api_gateway_rest_api.api.root_resource_id
  path_part = var.files[count.index].name
}

resource "aws_api_gateway_method" "proxy" {
  count = length(var.files)
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.root[count.index].id
  http_method = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda" {
  count = length(var.files)
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_method.proxy[count.index].resource_id
  http_method = aws_api_gateway_method.proxy[count.index].http_method
  integration_http_method = "POST"
  type = "AWS_PROXY"
  uri = aws_lambda_function.function[count.index].invoke_arn
}

resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_rest_api.api.root_resource_id
  http_method = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda_root" {
  count = length(var.files)
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_method.proxy_root.resource_id
  http_method = aws_api_gateway_method.proxy_root.http_method
  integration_http_method = "POST"
  type = "AWS_PROXY"
  uri = aws_lambda_function.function[count.index].invoke_arn
}

resource "aws_api_gateway_deployment" "api" {
  depends_on = [
    aws_api_gateway_integration.lambda,
    aws_api_gateway_integration.lambda_root,
  ]
  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name = "jokes"
}
