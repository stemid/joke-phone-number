variable "files" {
  type = list(object({
    filename = string
    name = string
  }))
}

variable "aws_default_region" {
  type = string
}
