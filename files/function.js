'use strict';

exports.handler = async (event) => {
    const response = {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            play: "${bucket_url}/${filename}",
            skippable: false
        })
    };
    return response
};
