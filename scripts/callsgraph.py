import sys
from datetime import datetime, timedelta

import click
import numpy
import requests
import matplotlib.dates as mdates
from matplotlib import pyplot


@click.command()
@click.option(
    '--user',
    required=True,
    type=str
)
@click.option(
    '--password',
    required=True,
    type=str
)
@click.option(
    '--save',
    is_flag=True,
    default=False
)
@click.option(
    '--output',
    type=click.File('wb'),
    default=open('graph.png', 'wb'),
    show_default=True
)
def plot_graph(user, password, save, output):
    end_time = datetime.now() - timedelta(days=5)

    # Request JSON from API
    r = requests.get(
        'https://api.46elks.com/a1/calls',
        auth=(user, password),
        params={
            'end': end_time.isoformat(),
            'start': datetime.now().isoformat(),
            'limit': 100
        }
    )

    # Setup locators and formatter for matplotlib
    hours = mdates.HourLocator()
    days = mdates.DayLocator()
    days_format = mdates.DateFormatter('%m/%d')

    try:
        calls = r.json()
    except Exception as e:
        print(str(e))
        print(r.text)
        sys.exit(-1)

    if not len(calls):
        print('No calls returned')
        sys.exit(-1)

    x = numpy.array(
        [datetime.fromisoformat(c['created']) for c in calls['data']]
    )

    # Just generate some value to place the event
    y = numpy.array([1 for i in range(0, len(calls['data']))])

    figure, ax = pyplot.subplots()
    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_major_formatter(days_format)
    ax.xaxis.set_minor_locator(hours)
    pyplot.plot_date(x, y, markersize=3.2)

    # Force a larger width of the resulting image
    figure.set_size_inches(6, 2)

    # Hide Y-axis legend text
    pyplot.yticks([])

    if save:
        pyplot.savefig(
            output,
            bbox_inches='tight',
            dpi=100
        )
    else:
        pyplot.show()


if __name__ == '__main__':
    plot_graph()
